from graphics import *


def main():

    win = GraphWin("Tęczowe góry i trawka (?)", 800, 600, autoflush=False)
    win.setBackground("lightblue")

    x0 = (5 * (-0.5 * win.winfo_screenwidth() + win.winfo_pointerx()))
    y0 = (5 * (-0.5 * win.winfo_screenheight() + win.winfo_pointery()))

    a = -350
    n = 0

    while True:
        x = -x0 + (5*(-0.5 * win.winfo_screenwidth() + win.winfo_pointerx()))
        y = -y0 + (5*(-0.5 * win.winfo_screenheight() + win.winfo_pointery()))

        slonce = Circle(Point(580+0.001*x, 80+0.001*y), 50)
        slonce.setFill("yellow1")
        slonce.draw(win)

        middleMountain = Polygon(Point(200+0.005*x, 550+0.005*y), Point(600+0.005*x, 550+0.005*y), Point(400+0.005*x, 100+0.005*y))
        middleMountain.setFill("red")
        middleMountain.setWidth(0)
        middleMountain.draw(win)

        leftMountain = Polygon(Point(-100+0.01*x, 550+0.01*y), Point(450+0.01*x, 550+0.01*y), Point(175+0.01*x, 150+0.01*y))
        leftMountain.setFill("green2")
        leftMountain.draw(win)

        rightMountain = Polygon(Point(350+0.01*x, 550+0.01*y), Point(900+0.01*x, 550+0.01*y), Point(625+0.01*x, 150+0.01*y))
        rightMountain.setFill("blue1")
        rightMountain.draw(win)

        trawa = Rectangle(Point(-400+0.03*x, 450+0.03*y), Point(1000+0.03*x, 900+0.03*y))
        trawa.setFill("lightgreen")
        trawa.draw(win)

        t01 = Line(Point(30 + a + 0.001*x, 30 + 0.001*y), Point(80 + a + 0.001*x, 60 + 0.001*y))
        t02 = Line(Point(80 + a + 0.001*x, 60 + 0.001*y), Point(130 + a + 0.001*x, 30 + 0.001*y))
        t01.setWidth(2)
        t02.setWidth(2)
        t01.draw(win)
        t02.draw(win)

        t11 = Line(Point(120 + a + 0.001*x, 90 + 0.001*y), Point(170 + a + 0.001*x, 120 + 0.001*y))
        t12 = Line(Point(170 + a + 0.001*x, 120 + 0.001*y), Point(220 + a + 0.001*x, 90 + 0.001*y))
        t11.setWidth(2)
        t12.setWidth(2)
        t11.draw(win)
        t12.draw(win)

        t21 = Line(Point(210 + a + 0.001*x, 150 + 0.001*y), Point(260 + a + 0.001*x, 180 + 0.001*y))
        t22 = Line(Point(260 + a + 0.001*x, 180 + 0.001*y), Point(310 + a + 0.001*x, 150 + 0.001*y))
        t21.setWidth(2)
        t22.setWidth(2)
        t21.draw(win)
        t22.draw(win)

        a+=10

        n += 1

        if n == 120:
            a=-350
            n = 0

        trawka = []

        for i in range(0, 450):
            trawka.append(Rectangle(Point(-300 + 5 * i + 0.03*x, 425+0.03*y), Point(-300 + 5 * i + 2 + 0.03*x, 450+0.03*y)))
            trawka[i].setFill("green4")
            trawka[i].setWidth(0)
            trawka[i].draw(win)

        update(10)

        slonce.undraw()
        trawa.undraw()
        middleMountain.undraw()
        leftMountain.undraw()
        middleMountain.undraw()
        rightMountain.undraw()

        t01.undraw()
        t02.undraw()
        t11.undraw()
        t12.undraw()
        t21.undraw()
        t22.undraw()

        for i in range(0, 250):
            trawka[i].undraw()


main()

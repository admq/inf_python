# inf_python

Repozytorium zawierające mniejsze projekty informatyczne realizowane przy użyciu Python'a. 

## /assignments

Folder zawiera kod do zadań informatycznych ze strony http://nifty.stanford.edu/. 

### /assignments/motion_parallax

*Motion Parallax is a programming assignment (PA) in which students create a scene that emulates the effect of motion parallax. The task is to write a program that draws a landscape scene on a canvas. As the mouse is moves across the canvas, the perspective of the scene should change using parallax principles.*

http://nifty.stanford.edu/2019/dicken-motion-parallax/